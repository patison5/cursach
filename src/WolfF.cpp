#include "WolfF.h"
#include "WolfM.h"
#include "Stats.h"

#include <cstdlib>
#include <vector>
#include <ctime>

#include <iostream>

using namespace std;

WolfF::WolfF()
{
    //ctor
}


void WolfF :: move (vector <Cell *>  around_cell,
                    list <WolfF> & _wolvesF,
                    list <WolfM> & _wolvesM,
                    list <WolfF>  :: iterator  wolfF_it,
                    Stats * stats) {

    vector <Cell *> :: iterator iter;
    iter = around_cell.begin();

    bool isRabbit = false;
    bool isWolfM  = false;
    bool isWolfF  = false;

    int choice = 0;

    //���� ����� ��������� - ��������, ����� - ����!
    if (health == 0) {
        //cout << endl << "wolf dead! ";
        die(_wolvesF, wolfF_it);
        stats -> increaseWolvesCount(); //����������� ������� ����������
    } else {

        health --;

        //������ ��� ������ �������
        Cell * temporaryCell = _cell;

        for (int i  = 0; i < around_cell.size(); i++) {
            if ((*around_cell[i]).rabbit != NULL) {
                isRabbit = true; //������� �����
                break;
            }

            if ((*around_cell[i]).wolfF!= NULL) { isWolfF = true; }  //������, ���� �� ��������� �� �������� �������
            if ((*around_cell[i]).wolfM!= NULL) { isWolfM = true; }  //������, ���� �� ��������� �� �������� �����

            choice++;
        }

        //����� ��������� �������� �����
        if (isRabbit) {

            if (around_cell[choice] -> rabbit != NULL) {
                //cout << "find rabbit!   ";

                //���� ������ �� ������ ����, ������ �� ����� � ��� ����������..... ������ ������?
                //around_cell[choice] -> rabbit -> die();
                //(*(*around_cell[choice]).rabbit).die();
            }


            (* _cell).wolfF = NULL;
            (*around_cell[choice]).wolfF = this;
            _cell = around_cell[choice];

            health = 10;
        }

        else if (!isWolfM) {
             //���� ������ �� ����� - ��������� �� ����...
            choice = rand() % around_cell.size();

            (* _cell).wolfF = NULL;
            (*around_cell[choice]).wolfF = this;
            _cell = around_cell[choice];

            born(temporaryCell, _wolvesF, _wolvesM);
            //cout << "Wolf was born!" << endl;

        } else {

            //���� ������ �� ����� - ������ ��������� �� ����...
            choice = rand() % around_cell.size();

            (* _cell).wolfF = NULL;
            (*around_cell[choice]).wolfF = this;
            _cell = around_cell[choice];
        }

        //cout << "WolfF health: " << health << endl;

    }
}


void WolfF :: die (list <WolfF> & _wolvesF, list <WolfF>  :: iterator  wolfF_it) {
    (* _cell).wolfF = NULL;
    wolfF_it->isDead = true; //������� ����� =)
    _wolvesF.erase(wolfF_it);
}


void WolfF :: born (Cell * temporaryCell, list <WolfF> & _wolvesF, list <WolfM> & _wolvesM) {

    //�����������, ��� ������� ����, ������� ��� ������ �� ����� ���������
    if ((rand() % 3) == 1) {
        WolfF obj;
        WolfF * objr = new WolfF; //������ �� ������ �����

        objr = &obj;

        obj._cell = temporaryCell;
        (*temporaryCell).c_set(objr);

        _wolvesF.push_back(obj);

    } else if ((rand() % 3) == 2)  {
        WolfM obj;
        WolfM * objr = new WolfM; //������ �� ������ �����

        objr = &obj;

        obj._cell = temporaryCell;
        (*temporaryCell).c_set(objr);

        _wolvesM.push_back(obj);
    }
}

