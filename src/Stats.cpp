#include "Stats.h"
#include "Output.h"

Stats::Stats() {
    this -> wolvesCount  = 0;
    this -> rabbitsCount = 0;

    this -> name = "stats";
    this -> lvl = 1;
}

Stats::Stats(int lvl) {
    this -> wolvesCount  = 0;
    this -> rabbitsCount = 0;

    this -> name = "stats";
    this -> lvl = lvl;
}

Stats * Stats :: getStats() {
    return this;
}


void Stats :: increaseWolvesCount() {
    this -> wolvesCount++;
}

void Stats :: increaseRabbitCount() {
    this -> rabbitsCount++;
}

int Stats :: getRabbitCount() {
    return rabbitsCount;
}

int Stats :: getWolvesCount() {
    return wolvesCount;
}

void Stats :: showTree () {
    output -> log("  Stats\n");
}


Stats * Stats :: getObjectR() {
    return this;
}
