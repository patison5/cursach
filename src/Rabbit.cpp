#include "Rabbit.h"
#include "Stats.h"

#include <vector>
#include <list>
#include <cstdlib>
#include <ctime>

#include <iostream>

using namespace std;

Rabbit::Rabbit() {}


void Rabbit :: move (vector <Cell *>  around_cell,
                     list <Rabbit> & _rabbits,
                     list <Rabbit> :: iterator rabbit_it,
                     Stats * stats) {

    if (_cell -> wolfF != NULL || _cell -> wolfM != NULL  && !isDead) {
        this -> isDead = true;
        health--;

        die(_rabbits, rabbit_it);

        stats -> increaseRabbitCount(); //����������� ������� ����������
    }

    if (!isDead) {

        int choice = rand() % around_cell.size();


        //������ ��� ������ �������
        Cell * temporaryCell = _cell;

        //��������� �������! ������� ����� ������ �������� �� ������ � ��� ������, ��� ���� ����� ����� �����
        if ((*around_cell[choice]).rabbit == NULL) {
             //������������� � ����������� ������
            (* _cell).rabbit = NULL;
            (*around_cell[choice]).rabbit = this;
            _cell = around_cell[choice];
        }

        if (rand() % 10 == 1) {
            born(temporaryCell, _rabbits);
        }

    }
}

void Rabbit :: die (list <Rabbit> & _rabbits, list <Rabbit> :: iterator rabbit_it) {
    (* _cell).rabbit = NULL;
    //_rabbits.erase(rabbit_it);
}

void Rabbit :: born (Cell * temporaryCell, list <Rabbit> & _rabbits) {
    Rabbit obj;
    Rabbit * objr = new Rabbit; //������ �� ������ �����

    objr = &obj;

    obj._cell = temporaryCell;
    (*temporaryCell).c_set(objr);

    _rabbits.push_back(obj);
}
