#include "Cell.h"
#include <cstddef>

#include <iostream>
using namespace std;


Cell::Cell() {}

Cell * Cell :: c_get_r () {
    return this;
}

void Cell :: c_set(Rabbit * rabb) {
    rabbit = rabb;
}

void Cell :: c_set(WolfF * wolf){
    wolfF = wolf;
}

void Cell :: c_set(WolfM * wolf){
    wolfM = wolf;
}



void Cell :: sayHello() {
    cout << endl <<  "Hello" << endl;
}
