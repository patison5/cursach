#include "WolfM.h"
#include "Stats.h"

#include <cstdlib>
#include <ctime>

#include <iostream>

using namespace std;

WolfM::WolfM() {}


void WolfM :: move (vector <Cell *>  around_cell,
                    list <WolfM> & _wolvesF,
                    list <WolfM>  :: iterator  wolfF_it,
                    Stats * stats) {

    vector <Cell *> :: iterator iter;
    iter = around_cell.begin();

    bool isRabbit = false;
    bool isWolfM  = false;
    bool isWolfF  = false;

    int choice = 0;

    //���� ����� ��������� - ��������, ����� - ����!
    if (health == 0) {
        die(_wolvesF, wolfF_it);
        stats -> increaseWolvesCount(); //����������� ������� ����������
    } else {

        health --;

        for (int i  = 0; i < around_cell.size(); i++) {
            if ((*around_cell[i]).rabbit != NULL) {
                isRabbit = true; //������� �����
                break;
            }

            if ((*around_cell[i]).wolfF!= NULL) { isWolfF = true; }  //������, ���� �� ��������� �� �������� �������
            if ((*around_cell[i]).wolfM!= NULL) { isWolfM = true; }  //������, ���� �� ��������� �� �������� �����

            choice++;
        }

        //����� ��������� �������� �����
        if (isRabbit) {

            if (around_cell[choice] -> rabbit != NULL) {
                //cout << "find rabbit!  " ;

                //���� ������ �� ������ ����, ������ �� ����� � ��� ����������..... ������ ������?
                //around_cell[choice] -> rabbit -> die();
                //(*(*around_cell[choice]).rabbit).die();
            }


            (* _cell).wolfM = NULL;
            (*around_cell[choice]).wolfM = this;
            _cell = around_cell[choice];

            health = 10;
        }

        else if (!isWolfF || !isWolfM) {
             //���� ������ �� ����� - ��������� �� ����...
            choice = rand() % around_cell.size();

            (* _cell).wolfM = NULL;
            (*around_cell[choice]).wolfM = this;
            _cell = around_cell[choice];

        } else {

            //���� ������ �� ����� - ������ ��������� �� ����...
            choice = rand() % around_cell.size();

            (* _cell).wolfM = NULL;
            (*around_cell[choice]).wolfM = this;
            _cell = around_cell[choice];
        }


        //cout << "WolfM health: " << health << endl;

    }
}


void WolfM :: die (list <WolfM> & _wolvesF, list <WolfM>  :: iterator  wolfF_it) {
    (* _cell).wolfM = NULL;
    wolfF_it->isDead = true; //������� ����� =)
    wolfF_it->_cell = NULL;
    _wolvesF.erase(wolfF_it);
}
