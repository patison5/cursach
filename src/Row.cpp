#include <iostream>
#include "Row.h"
#include "Cell.h"
#include <vector>

using namespace std;

Row::Row() {}


Cell Row :: c_get(int y) {
    return _cells[y];
}

Cell * Row :: c_get_r (int y) {
    return  _cells[y].c_get_r();
}

void Row :: c_set (int y, WolfF * value) {
    _cells[y].c_set(value);
}

void Row :: c_set (int y, WolfM * value) {
    _cells[y].c_set(value);
}

void Row :: c_set (int y, Rabbit * value) {
    _cells[y].c_set(value);
}

void Row :: createWorld(int _worldY, int x) {
    for (int i = 0; i < _worldY; i++) {
        Cell cell;

        cell.PosX = x; //��������� ������-������ �� �������� � �������
        cell.PosY = i;

        _cells.push_back(cell);
    }
}


void Row :: renderWorld(int _worldY, string & res) {

    //for (int i = 0; i < _worldY; i++) {
      //  (_cells[i].rabbit != NULL) ? cout << "RR " :
        //    (_cells[i].wolfM != NULL) ? cout << "Wm " :
          //      (_cells[i].wolfF != NULL) ? cout << "Wf " : cout << "-- ";

    for (int i = 0; i < _worldY; i++) {
        (_cells[i].rock) ? res += "**" :
            (_cells[i].wolfM != NULL) ? res += "Wm" :
                (_cells[i].wolfF != NULL) ? res += "Wf" :
                    (_cells[i].rabbit != NULL) ? res += "RR" : res += "  ";
    }
    res += "\n";

    //cout << res;
}

void Row :: createRocks (int y) {
    _cells[y].rock = 1;
}
