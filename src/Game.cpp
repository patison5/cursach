#include <list>
#include <vector>

#include "Game.h"
#include "Rabbit.h"
#include "Wolves.h"
#include "Stats.h"
#include "Output.h"
#include "Input.h"

#include "Test_stats.h"

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

Game::Game() {
    _grid.createWorld();
}

void Game :: ApplySetup() {
    int rabbit_count,
        wolfM_count,
        wolfF_count;

    //Output out;
    //output = &out;

    output -> log("\nEnter number of Rabbits: ");
    rabbit_count = input -> getNumber();

    output -> log("\nEnter number of wolves (male): ");
    wolfM_count = input -> getNumber();

    output -> log("\nEnter number of wolves (female): ");
    wolfF_count = input -> getNumber();

    //��������, ����� �� ������� ������ ���
    rabbit_count = 15;
    wolfF_count  = 5;
    wolfM_count  = 5;

    setupFor<Rabbit>(rabbit_count, _rabbits);
    setupFor<WolfF>(wolfF_count, _wolvesF);
    setupFor<WolfM>(wolfM_count, _wolvesM);

    cout << endl;
    cout << _wolvesF.size()  << " Wolves  (F) should be added to the map" << endl;
    cout << _wolvesM.size()  << " Wolves  (M) should be added to the map" << endl;
    cout << _rabbits.size()  << " Rabbits (-) should be added to the map" << endl  << endl;
}



template <class T, typename TT>
void Game :: setupFor (int unitsCount, list <TT> & _list) {

    //��������������� ������ ��� ����������....
    Stats stat;
    stats = &stat;

    isGameEnd = false;

    //_grid.createRocks();

    for (int i = 0; i < unitsCount; i++) {
        T obj;
        T * objr = new T; //������ �� ������ �����

        objr = &obj;

        // ��������� � ��� ���...
        int x = rand() % 30;
        int y = rand() % 30;

        Cell * temporaryCell = _grid.c_get_r(x,y);

        //temporaryCell = _grid.checkCell(x,y);

        obj._cell = temporaryCell;

        (*temporaryCell).c_set(objr);

        //_grid.c_set(x,y, objr); //������ ������� ���������� (��������� ����� ������ ����)

        if ((*obj._cell).rabbit != NULL) {
            cout << "������ ��������!";
        }

        if ((*obj._cell).wolfM != NULL) {
            cout << "����   ��������!";
        }
        if ((*obj._cell).wolfF != NULL) {
            cout << "����   ��������!";
        }

        cout << "(x,y):(" << (*obj._cell).PosX << "," << (*obj._cell).PosY  << ")" << endl;

        _list.push_back(obj);
    }
}

void Game :: renderWorld() {
    _grid.renderWorld();
}



void Game :: StartGame () {

    Stats st;
    stats = &st;

    showTree();

    //������� ����

    while (!isGameEnd) {
        system("cls"); // ������� �����

        moveAnimals();
        cout << endl;

        renderWorld();

        if (allRabbitsDead() || allWolvesMDead() || allWolvesFDead())
            isGameEnd = true;

        _sleep(100);
    }

    cout << endl << "THE SIMULATION STOPED!" << endl;

    cout << "Dead rabbits: " << stats -> getRabbitCount() << endl;
    cout << "Dead Wolves:  " << stats -> getWolvesCount() << endl;
}


bool Game :: allRabbitsDead () {
    for (rabbit_it = _rabbits.begin(); rabbit_it != _rabbits.end(); rabbit_it++) {
        if (!rabbit_it->isDead)
            return 0;
    }
    return 1;
}
bool Game :: allWolvesMDead () {
    for (wolfM_it = _wolvesM.begin(); wolfM_it != _wolvesM.end(); wolfM_it++) {
        if (!wolfM_it->isDead)
            return 0;
    }
    return 1;
}

bool Game :: allWolvesFDead () {
    for (wolfF_it = _wolvesF.begin(); wolfF_it != _wolvesF.end(); wolfF_it++) {
        if (!wolfF_it->isDead)
            return 0;
    }
    return 1;
}




void Game :: moveAnimals () {
    for (rabbit_it = _rabbits.begin(); rabbit_it != _rabbits.end(); rabbit_it++) {
        vector <Cell *>  around_cell;

        _grid.getArrowndCells( rabbit_it -> _cell -> PosX,
                               rabbit_it -> _cell -> PosY,
                               around_cell);

        //rabbit_it -> _cell -> rabbit -> die();
//        (*(*(*rabbit_it)._cell).rabbit).die();


        (*rabbit_it).move(around_cell, _rabbits, rabbit_it, stats);
    }

    for (wolfF_it = _wolvesF.begin(); wolfF_it != _wolvesF.end(); wolfF_it++) {
        vector <Cell *> around_cell;

        _grid.getArrowndCells( wolfF_it -> _cell -> PosX,
                               wolfF_it -> _cell -> PosY,
                               around_cell);

        //cout << "�������� �����  ��� ������ (" << wolfF_it -> _cell -> PosX << "," << wolfF_it -> _cell -> PosY << "): " << arrownd_cell.size() << endl;

        (*wolfF_it).move(around_cell, _wolvesF, _wolvesM, wolfF_it, stats);
    }

    for (wolfM_it = _wolvesM.begin(); wolfM_it != _wolvesM.end(); wolfM_it++) {
        vector <Cell *> around_cell;

        _grid.getArrowndCells( wolfM_it -> _cell -> PosX,
                               wolfM_it -> _cell -> PosY,
                               around_cell);

        //cout << "�������� �����  ��� ������ (" << wolfM_it -> _cell -> PosX << "," << wolfM_it -> _cell -> PosY << "): " << arrownd_cell.size() << endl;

        (*wolfM_it).move(around_cell, _wolvesM, wolfM_it, stats);
    }
}

void Game :: showTree() {
    output -> log(name);

    Stats * st = new Stats (1);
    stats = st;

    Output * out = new Output (1);
    output = out;

    Input * in = new Input(1);
    input = in;

    Test_stats * test1 = new Test_stats (2);
    Test_stats * test2 = new Test_stats (2);
    Test_stats * test3 = new Test_stats (2);

    _listOfObjects.push_back(stats);
    _listOfObjects.push_back(output);
    _listOfObjects.push_back(input);
    _listOfObjects.push_back(test1);
    _listOfObjects.push_back(test2);
    _listOfObjects.push_back(test3);




    for (obj_it = _listOfObjects.begin(); obj_it != _listOfObjects.end(); obj_it++) {

        for (int i = 0; i < (*obj_it) -> lvl; i++) {
            cout << "  ";
        }
        cout << (*obj_it) -> name << endl;
    }

    string k;
    cout << "continue? ";
    cin >> k;
}
