#include <iostream>
#include <vector>

#include "Grid.h"
#include "Cell.h"
#include "Output.h"

#include <cstdlib>
#include <ctime>

using namespace std;

Grid::Grid() {
    _worldX = 30;
    _worldY = 30;
}



Cell Grid :: c_get (int x,int y) {
    return _rows[x].c_get(y);
}

Cell * Grid :: c_get_r (int x,int y) {
    return _rows[x].c_get_r(y);
}

void Grid :: c_set (int x, int y, WolfM * value) {
    _rows[x].c_set(y, value);
}
void Grid :: c_set (int x, int y, WolfF * value) {
    _rows[x].c_set(y, value);
}

void Grid :: c_set (int x, int y, Rabbit * value) {
    _rows[x].c_set(y, value);
}


void Grid :: createWorld() {
    for (int i = 0; i < _worldX; i++) {
        Row row;
        row.createWorld(_worldY, i);
        _rows.push_back(row);
    }
}

void Grid :: renderWorld() {
    string str = "";

    for (int i = 0; i < _worldX; i++) {
        _rows[i].renderWorld(_worldY, str);
    }

    std::ios::sync_with_stdio(false);

    output -> log(str);
}


//��� ��� ������.... �������� ��������� ����� ������
Cell * Grid :: checkCell (int x, int y) {
    Cell * temporaryCell;
    (*temporaryCell) = c_get(x,y);

    if ((temporaryCell -> rabbit == NULL) && (temporaryCell -> wolfM == NULL)) {
        return temporaryCell;
    } else { return NULL; }
}

void Grid :: getArrowndCells (int x, int y, vector <Cell *> & arr_cell) {
    for (int i = x-1; i <= x+1; i++)
        for (int j = y - 1; j <= y + 1; j++)
            if ((i >= 0) && (i < _worldX) && (j >= 0) && (j < _worldY) && (i != j))
                arr_cell.push_back(c_get_r(i,j));
}

void Grid :: createRocks() {
    // ��������� � ��� ���...
    srand(time(NULL));

    int r = 4;

    int x = rand() % _worldX;
    int y = rand() % _worldY;

    //output -> log("Rock created! (x: " + x + " y: " + y);

    for (int i = 0; i < r; i++)
        for (int j = 0; j < r; j++)
            if (((i*i + j*j) < r*r) && (x+i < _worldX) && (x-i >= 0) && (y+i < _worldY) && (y-i >= 0))  {
                cout << "Rock created! (x: " << x << " y: " << y<< ")" << endl;


                _rows[i+x].createRocks(j+y);
                _rows[x-i].createRocks(y-j);
                _rows[x-i].createRocks(j+y);
                _rows[i+x].createRocks(y-j);

            }


    cout << "Rock created! (x: "  << endl;
}
