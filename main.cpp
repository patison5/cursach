#include <iostream>
#include <ctime>
#include <cstdlib>

#include "Game.h"

using namespace std;

int main() {
    srand(time(0));
	setlocale(LC_ALL, "rus");

    cout << endl << "**********////// WELCOME TO MY GAME! \\\\\\\\\\\\\********** " << endl;

    Game game;

    string str = "Y";

    while (str != "N") {
        game.ApplySetup();
        game.StartGame();

        cout << endl << "Do it again? Y/N: ";
        cin >> str;
    }


    return 0;
}
