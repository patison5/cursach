#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <list>
#include <vector>
#include "Grid.h"

#include "Rabbit.h"
#include "Wolves.h"
#include "WolfF.h"
#include "WolfM.h"

using namespace std;

class Stats;
class Output;
class Input;

class Game {
    public:
        Game();
        Grid _grid;

        int lvl;
        string name = "game\n";

        Stats  * stats;
        Output * output;
        Input  * input;

        bool isGameEnd = false;

        list <WolfM>  _wolvesM;
        list <WolfF>  _wolvesF;
        list <Rabbit> _rabbits;

        list <Rabbit> :: iterator rabbit_it;
        list <WolfM>  :: iterator  wolfM_it;
        list <WolfF>  :: iterator  wolfF_it;

        void ApplySetup();

        void StartGame();

        void renderWorld();

        void showTree();

    private:

        template <class T, typename TT>
        void setupFor (int N, list <TT> & _list);
        void moveAnimals();

        bool allRabbitsDead();
        bool allWolvesMDead();
        bool allWolvesFDead();

        vector <Game *> _listOfObjects;
        vector <Game *> :: iterator obj_it;
};

#endif // GAME_H
