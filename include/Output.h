#ifndef OUTPUT_H
#define OUTPUT_H

#include <Game.h>

#include <string>

using namespace std;

class Output : public Game {
    public:
        Output();

        Output(int lvl);

        void log (string str);

        void showTree();

    protected:

    private:
};

#endif // OUTPUT_H
