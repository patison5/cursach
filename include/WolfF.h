#ifndef WOLFF_H
#define WOLFF_H

#include "Wolves.h"
#include "Cell.h"

#include <vector>
#include <list>

using namespace std;

class Wolves;
class Stats;

class WolfF : public Wolves {
    public:
        WolfF();

        Cell * _cell;

        void move (vector <Cell *>  around_cell,
                      list <WolfF> & _wolvesF,
                      list <WolfM> & _wolvesM,
                      list <WolfF> :: iterator  wolfF_it,
                      Stats * stats);

        void die(list <WolfF> & _wolvesF,
                 list <WolfF> :: iterator  wolfF_it);

        private:
            void born(Cell * temporaryCell, list <WolfF> & _wolvesF, list <WolfM> & _wolvesM);
};

#endif // WOLFF_H
