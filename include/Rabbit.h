#ifndef RABBIT_H
#define RABBIT_H

#include <Unit.h>
#include "Cell.h"

#include <vector>
#include <list>

using namespace std;

class Cell;
class Stats;

class Rabbit : public Unit {
    public:
        Rabbit();

        bool isDead = false;

        Cell * _cell;

        void move (vector <Cell *>  around_cell,
                     list <Rabbit> & _rabbits,
                     list <Rabbit> :: iterator rabbit_it,
                     Stats * stats);

        void die (list <Rabbit> & _rabbits,
                  list <Rabbit> :: iterator rabbit_it);

    private:
        void born(Cell * temporaryCell, list <Rabbit> & _rabbits);
};

#endif // RABBIT_H
