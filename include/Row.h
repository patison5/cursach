#ifndef ROW_H
#define ROW_H

#include <iostream>
#include <vector>
#include "Cell.h"

using namespace std;

class Row {
    public:
        Row();

        Cell c_get(int y);
        Cell * c_get_r (int y);

        void c_set(int y, int value);

        void c_set (int y, WolfM  * value);
        void c_set (int y, WolfF  * value);
        void c_set (int y, Rabbit * value);

        void createWorld(int _worldY, int x);
        void renderWorld(int _worldY, string & str);

        void createRocks (int y);

    private:
        vector <Cell> _cells;
};

#endif // ROW_H


