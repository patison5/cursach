#ifndef WOLFM_H
#define WOLFM_H

#include <Wolves.h>
#include "Cell.h"

#include <vector>
#include <list>

using namespace std;

class Stats;

class WolfM : public Wolves {
    public:
        WolfM();

        Cell * _cell;

        void move (vector <Cell *>  around_cell,
                      list <WolfM> & _wolvesF,
                      list <WolfM>  :: iterator  wolfF_it,
                      Stats * stats);

        void die (list <WolfM> & _wolvesF,
                  list <WolfM>  :: iterator  wolfF_it);

    private:
};

#endif // WOLFM_H
