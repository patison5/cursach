#ifndef INPUT_H
#define INPUT_H

#include <Game.h>


class Input : public Game
{
    public:
        Input();
        Input(int lvl);

        int getNumber();

        void showTree();

    protected:

    private:
};

#endif // INPUT_H
