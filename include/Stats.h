#ifndef STATS_H
#define STATS_H

#include <Game.h>

#include <string>

using namespace std;

class Output;

class Stats : public Game {
    public:
        Stats();
        Stats(int lvl);

        Stats * getStats();
        Output * output;


        void increaseWolvesCount();
        void increaseRabbitCount();

        int getWolvesCount();
        int getRabbitCount();

        void showTree();

        Stats * getObjectR();

    private:
        int wolvesCount;
        int rabbitsCount;
};

#endif // STATS_H
