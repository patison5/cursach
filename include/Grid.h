#ifndef GRID_H
#define GRID_H

#include <iostream>
#include <vector>
#include "Row.h"

#include "Cell.h"
#include "Unit.h"

using namespace std;

class Output;

class Grid {
    public:
        Grid();

        Output * output;

        // Getters
        Cell c_get(int x,int y);
        Cell * c_get_r (int x, int y);

        // Setters
        void c_set(int x, int y, WolfF * value);
        void c_set(int x, int y, WolfM * value);
        void c_set(int x, int y, Rabbit * value);

        void createWorld();
        void renderWorld();
        void createRocks();

        void getArrowndCells (int x, int y, vector <Cell *> & arr_cell);


        Cell * checkCell(int x, int y); //���� ������, ������ ������

    private:
        int _worldX;
        int _worldY;

        vector <Row> _rows;
};

#endif // GRID_H
